# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

:scroll: is a function \
:package: is a module \
:memo: is a script

## [1.7.0]

### Changed

-	use `hostname` to get hostname instead of (windows-specific) `$env:COMPUTERNAME`
-	ignore global `TRACE` environment variable. `-trace` and `-log` are handled separately

## [1.6.0]

> Please read [UPGRADING.md](UPGRADING.md) carrefully

### Added

-	:scroll: `Write-ToLogFile`
-	:scroll: `Write-LogMessage`
-	:scroll: `Send-ToSysLog`

### Changed

### Deprecated

### Removed

### Fixed

### Security
