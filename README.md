# pwsh.fw.log

`PwSh.Fw.Log` add logging capabilities to a `PwSh.Fw` project. It can write messages to a file or send log to a syslog, all at the same time.